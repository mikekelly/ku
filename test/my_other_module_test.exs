defmodule MyOtherModuleTest do
  use ExUnit.Case
  doctest Ku

  test "make sure also_do_it doesn't crash" do
    {status} = MyOtherModule.also_do_it(%Message{})
    assert status === :ok
  end
end
