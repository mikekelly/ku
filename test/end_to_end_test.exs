defmodule EndToEndTest do
  use ExUnit.Case
  import Mock
  doctest Ku

  setup do
    test_pid = self()
    Ku.start(:dontcare, :dontcare)
    Ku.subscribe ~r/^foo\.bar$/, fn(msg) -> send(test_pid, consumer: "foo.bar", message: msg) end
    Ku.subscribe ~r/^foo\.*/, fn(msg) -> send(test_pid, consumer: "foo.*", message: msg) end
  end

  test "end to end foo.bar" do
    msg = %Message{ route: "foo.bar", body: %{bar: "baz"}, metadata: %{optional: "metadata object"} }
    Ku.publish msg.route, msg.body, msg.metadata
    assert_receive consumer: "foo.bar", message: msg
    assert_receive consumer: "foo.*", message: msg
  end

  test "end to end foo.lala" do
    msg = %Message{ route: "foo.lala", body: %{bar: "baz"}, metadata: %{optional: "metadata object"} }
    Ku.publish msg.route, msg.body, msg.metadata
    assert_receive consumer: "foo.*", message: msg
    refute_receive consumer: "foo.bar", message: msg
  end

  test "end to end unhandled_key" do
    msg = %Message{ route: "unhandled_key", body: %{bar: "baz"}, metadata: %{optional: "metadata object"} }
    Ku.publish msg.route, msg.body, msg.metadata
    refute_receive consumer: "foo.bar", message: msg
    refute_receive consumer: "foo.*", message: msg
  end
end
