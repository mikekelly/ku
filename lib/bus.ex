defmodule Bus do
  @moduledoc """
  Provides a publish/subscribe message bus. The implementation uses an
  association table associating regular expressions to the callback functions.

  Example table contents:
  [
    {~r/^foo\.bar$/, &MyModule.do_it/1},
    {~r/^foo\.*/, &MyOtherModule.also_do_it/1}
  ]
  """

  require Logger
  use GenServer

  @doc """
  Starts the publish/subscribe message bus
  """
  def start_link(name) do
    GenServer.start_link(__MODULE__, [], name: name)
  end

  @doc """
  Initialize message bus with route table or an empty list.
  """
  def init(route_table \\ []) do
    {:ok, route_table}
  end

  @doc """
  Publishes messages to the appropriate subscribers based out publisher
  route key and subscribers regex route key. This function calls each callback
  function in parallel based by using the Task.async.
  """
  def handle_cast({:publish, msg}, route_table) do
    route_table
    # filter keeps only regex matches
    |> Enum.filter(fn {regex_route_key, _} -> msg.route =~ regex_route_key end)
    # map executes subscriber callbacks in parallel and asynchronously
    |> Enum.each(&Task.async(fn -> elem(&1, 1).(msg) end))
    {:noreply, route_table}
  end

  @doc """
  Adds a subscribers to the message bus
  """
  def handle_call({:add, regex_route_key, callback}, _from, route_table) do
    {:reply, :ok, [{regex_route_key, callback}] ++ route_table}
  end
end
